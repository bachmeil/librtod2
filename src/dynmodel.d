
// WARNING: This file is more of a first stab at working with time series than a library for doing so
// Some things are clumsy

module rtod2.dynmodel;

private import rtod2.matrix, rtod2.r, rtod2.reg, rtod2.vector, rtod2.utils;
private import std.array, std.conv, std.exception, std.stdio;

struct TS {
  DoubleVector data;
  ulong start;
  ulong frequency;

  this(DoubleVector v, ulong[2] s, ulong f) {
    data = v;
    start = s[0]*f + s[1];
    frequency = f;
  }

  this(DoubleVector v, ulong s, ulong f) {
    data = v;
    start = s;
    frequency = f;
  }

  this(ulong length, ulong s, ulong f) {
    data = DoubleVector(to!int(length));
    start = s;
    frequency = f;
  }

  this(DoubleVector v, ulong s) {
    data = v;
    start = s;
    frequency = 1;
  }

  this(DoubleVector v, ulong[2] s) {
    data = v;
    start = s[0] + s[1];
    frequency = 1;
  }

  this(Robj rv) {
    assertR(rv.class == "ts", "In TS constructor: Cannot convert non-ts object to TS");
    data = RVector(rv).vec;
    auto tsp = rv.tsp;
    start = tsp[0];
    frequency = tsp[2];
  }

  // Indexing by element number has no meaning for a TS struct
  double opIndex(ulong d) { 
    assertR(d >= start, "Index on TS is before the start of the TS");
    assertR(d <= this.end, "Index on TS is after the end of the TS");
    return data[to!int(d-start)]; 
  }

  // Index is for the date as the element number has no meaning
  void opIndexAssign(double val, ulong d) {
    assertR(d >= start, "Index on TS is before the start of the TS");
    assertR(d <= this.end, "Index on TS is after the end of the TS");
    data[to!int(d-start)] = val;
  }

  // x[1990,3]
  double opIndex(ulong y, ulong p) {
    return this[y*frequency + p];
  }

  TS opSlice(ulong s, ulong e) {
    return TS(data[to!int(s-start)..to!int(e-start)], s, frequency);
  }
  
  TS opSlice(ulong[2] s, ulong[2] e) {
    return opSlice(s[0]*frequency+s[1], e[0]*frequency+e[1]);
  }

  ulong opDollar() {
    return start + data.length;
  }

  ulong length() { return data.length; }

  ulong end() { return start+data.length-1; }

  ulong[2] startDate() { return [start/frequency, start%frequency]; }

  ulong[2] endDate() { return [this.end/frequency, this.end%frequency]; }

  void print() {
    writeln("---------------------");
    write("Start:     ", this.startDate, "\n");
    write("End:       ", this.endDate, "\n");
    write("Frequency: ", frequency, "\n");
    writeln("---------------------");
    data.print();
  }

  TS opBinary(string op)(TS y) {
    static if (op == "+") {
      auto cd = commonDates(this, y);
      return add(this[cd[0]..cd[1]], y[cd[0]..cd[1]]);
    }
    static if (op == "-") {
      auto cd = commonDates(this, y);
      return subtract(this[cd[0]..cd[1]], y[cd[0]..cd[1]]);
    }
    static if (op == "*") {
      auto cd = commonDates(this, y);
      return times(this[cd[0]..cd[1]], y[cd[0]..cd[1]]);
    }
    static if (op == "/") {
      auto cd = commonDates(this, y);
      return divide(this[cd[0]..cd[1]], y[cd[0]..cd[1]]);
    }
  }

  TS opBinary(string op)(ulong z) {
    static if (op == "^^") {
      auto result = DoubleVector(data.length);
      foreach(int ii; 0..data.length) {
        result[ii] = data[ii] ^^ z;
      }
      return TS(result, start, frequency);
    }
  }
}

Robj robj(TS x) {
  return RVector(x.data).robj;
}

double sum(TS x) {
  return rtod2.vector.sum(x.data);
}

double prod(TS x) {
  return rtod2.vector.prod(x.data);
}

double[] array(TS x) {
  return rtod2.vector.array(x.data);
}

TS dup(TS x) {
  return TS(rtod2.vector.dup(x.data), x.start, x.frequency);
}

private TS add(TS x, TS y) {
  auto result = DoubleVector(to!int(x.length)); 
  foreach(ii; 0..to!int(x.length)) {
    result[ii] = x.data[ii] + y.data[ii];
  }
  return TS(result, x.start, x.frequency);
}

private TS subtract(TS x, TS y) {
  auto result = DoubleVector(to!int(x.length)); 
  foreach(ii; 0..to!int(x.length)) {
    result[ii] = x.data[ii] - y.data[ii];
  }
  return TS(result, x.start, x.frequency);
}

private TS times(TS x, TS y) {
  auto result = DoubleVector(to!int(x.length)); 
  foreach(ii; 0..to!int(x.length)) {
    result[ii] = x.data[ii] * y.data[ii];
  }
  return TS(result, x.start, x.frequency);
}

private TS divide(TS x, TS y) {
  auto result = DoubleVector(to!int(x.length)); 
  foreach(ii; 0..to!int(x.length)) {
    result[ii] = x.data[ii] / y.data[ii];
  }
  return TS(result, x.start, x.frequency);
}

TS lag(TS x, long k=1) {
  return TS(x.data, x.start+k, x.frequency);
}

TS lead(TS x, long k=1) {
  return x.lag(-k);
}

TS window(TS x, ulong t0, ulong t1) {
  assertR(t0 >= x.start, "struct TS || function window || start date is before series start date");
  assertR(t1 <= x.end, "struct TS || function window || end date is after series end date");
  return x[t0..t1+1];
}

// x.window([1990,1], [2012,12])
TS window(TS x, ulong[2] s, ulong[2] e) {
  auto t0 = s[0]*x.frequency + s[1];
  auto t1 = e[0]*x.frequency + e[1];
  return window(x, t0, t1);
}

// x.starting(1999,7)
TS starting(TS x, ulong y, ulong p) {
  auto t0 = y*x.frequency + p;
  return x.window(t0, x.end);
} 

// x.ending(2012,12)
TS ending(TS x, ulong y, ulong p) {
  auto t1 = y*x.frequency + p;
  return x.window(x.start, t1);
}

struct MTS {
  TS[string] data;
  immutable ulong frequency;

  this(TS x, string name) {
    data[name] = x;
    frequency = x.frequency;
  }
  
  this(ulong f) {
    frequency = f;
  }

  void print() {
    foreach(name; data.byKey()) {
      writeln("\n", name);
      data[name].print();
    }
  }

  void opIndexAssign(TS x, string name) {
    assertR(x.frequency == this.frequency, "struct MTS || function opIndexAssign || Cannot have mixed frequency MTS");
    data[name] = x;
  }

  TS opIndex(string name) {
    return data[name];
  }
}

auto byValue(MTS x) {
  return x.data.byValue();
}

auto byKey(MTS x) {
  return x.data.byKey();
}

MTS window(MTS x, ulong t0, ulong t1) {
 auto result = MTS(x.frequency);
 foreach(name; byKey(x)) { result[name] = window(x[name], t0, t1); }
 return result;
}

MTS window(MTS x, ulong[2] s, ulong[2] e) {
  auto t0 = s[0]*x.frequency + s[1];
  auto t1 = e[0]*x.frequency + e[1];
  return window(x, t0, t1);
}

MTS starting(MTS x, ulong y, ulong p) {
  auto result = MTS(x.frequency);
  foreach(name; byKey(x)) { result[name] = starting(x[name], y, p); }
  return result;
}

MTS ending(MTS x, ulong y, ulong p) {
  auto result = MTS(x.frequency);
  foreach(name; byKey(x)) { result[name] = ending(x[name], y, p); }
  return result;
}

MTS intersect(MTS x) {
  auto cd = commonDates(x);
  return window(x, cd[0], cd[1]);
}

struct Term {
  string name;
  ulong lag;

  Term[] opBinary(string op)(Term[] x) {
    if (op == "~") {
      Term[] result;
      result.reserve(x.length+1);
      result ~= this;
      result ~= x;
      return result;
    }
  }
}

struct DynEq {
  Term lhs;
  Term[] rhsvars;
  bool intercept = true;

  void opOpAssign(string op)(Term x) {
    if (op == "+=") {
      rhsvars ~= x;
    }
  }

  void opOpAssign(string op)(Term[] x) {
    if (op == "+=") {
      rhsvars ~= x;
    }
  }
}

struct DynCoef {
  string name;
  ulong lag;
  double b;
}

struct DynFit {
  DynCoef[] coef;
  double intercept;
  ulong estStart;
  ulong estEnd;
  ulong frequency;
}

Term lag(string name, ulong k) { 
  return Term(name, k); 
}

Term[] lags(string name, ulong[] ks) {
  Term[] result;
  result.reserve(ks.length);
  foreach(k; ks) { 
    result ~= Term(name, k); 
  }
  return result;
}

TS[] rhs(MTS x, DynEq eq) {
  TS[] result;
  foreach(var; eq.rhsvars) { 
    result ~= lag(x[var.name], var.lag); 
  }
  return result;
}

TS[] rhs(MTS x, DynEq eq, ulong[2] dates) {
  TS[] result;
  foreach(var; eq.rhsvars) { 
    result ~= window(lag(x[var.name], var.lag), dates[0], dates[1]); 
  }
  return result;
}

TS lhs(MTS x, DynEq eq) {
  return lag(x[eq.lhs.name], eq.lhs.lag);
}

TS lhs(MTS x, DynEq eq, ulong[2] dates) {
  return window(lag(x[eq.lhs.name], eq.lhs.lag), dates[0], dates[1]);
}

ulong[2] commonDates(TS[] x) {
  ulong s=0, e=ulong.max;
  foreach(var; x) {
    s = imax(s, var.start);
    e = imin(e, var.end);
  }
  assertR(s <= e, "struct MTS || function commonDates(TS[] x) || No overlapping observations");
  return [s, e];
}

ulong[2] commonDates(TS x, TS y) {
  ulong s = imax(x.start, y.start);
  ulong e = imin(x.end, y.end);
  assertR(s <= e, "function commonDates(TS x, TS y) || No overlapping observations");
  return [s, e];
}

ulong[2] commonDates(MTS x) {
  ulong s=0, e=ulong.max;
  foreach(var; x.data) {
    s = imax(s, var.start);
    e = imin(s, var.end);
  }
  assertR(s <= e, "struct MTS || function commonDates(MTS x) || No overlapping observations");
  return [s, e];
}

ulong[2] commonDates(TS[] x, TS y) {
  auto cd = commonDates(x);
  cd[0] = imax(cd[0], y.start);
  cd[1] = imin(cd[1], y.end);
  assertR(cd[0] <= cd[1], "struct MTS || function commonDates(TS[] x, TS y) || No overlapping observations");
  return cd;
}

ulong[2] commonDates(TS y, TS[] x) {
  return commonDates(x, y);
}

ulong[2] commonDates(MTS x, TS y) {
  auto cd = x.commonDates;
  ulong s = imax(cd[0], y.start);
  ulong e = imin(cd[1], y.end);
  assertR(s <= e, "struct MTS || function commonDates(MTS x, TS y) || No overlapping observations");
  return [s, e];
}

ulong[2] commonDates(MTS x, DynEq eq) {
  return commonDates(x.rhs(eq), x.lhs(eq));
}

int nrow(TS[] x) {
  return to!int(x[0].length);
}

int ncol(TS[] x) {
  return to!int(x.length);
}

DoubleMatrix regressors(MTS x, DynEq eq, ulong[2] dates) {
  TS lhs = x.lhs(eq, dates);
  TS[] rhs = x.rhs(eq, dates);
  auto result = DoubleMatrix(rhs.nrow, rhs.ncol + to!int(eq.intercept));
  foreach(int ii, var; rhs) { 
    Col(result, ii) = var.data; 
  }
  if (eq.intercept) { Col(result, result.cols) = 1.0; }
  return result;
}

DoubleMatrix regressors(MTS x, DynEq eq) {
  TS lhs = x.lhs(eq);
  TS[] rhs = x.rhs(eq);
  return regressors(x, eq, commonDates(lhs, rhs));
}

// x.fillColumn(3, z);
private void fillColumn(DoubleMatrix x, int col, TS z) {
  assertR(col < x.cols, "fillColumn: col is greater than the number of columns in the matrix");
  foreach(ii; to!int(z.start)..to!int(z.end+1)) { 
    x[ii-to!int(z.start), col] = z[ii]; 
  }
}

private void fillColumn(DoubleMatrix x, int col, double z) {
  assertR(col < x.cols, "fillColumn: col is greater than the number of columns in the matrix");
  foreach(ii; 0..x.rows) { 
    x[ii, col] = z; 
  }
}

DoubleMatrix regressors(TS[] rhs, ulong[2] dates, bool intercept) {
  auto result = DoubleMatrix(to!int(dates[1]-dates[0]+1), rhs.ncol + to!int(intercept));
  foreach(int ii, var; rhs) { 
    result.fillColumn(ii, window(var, dates[0], dates[1])); 
  }
  if (intercept) { result.fillColumn(result.cols-1, 1.0); }
  return result;
}

TS[] intersect(MTS x, DynEq eq) {
  TS[] temp; // Holds all TS structs, with no restrictions on the time period
  foreach(var; eq.rhsvars) { 
    temp ~= lag(x[var.name], var.lag); 
  }
  TS[] result; 
  result.reserve(temp.length);
  auto cd = temp.commonDates;
  foreach(var; temp) { 
    result ~= window(var, cd[0], cd[1]); 
  }
  return result;
}

TS[] intersect(MTS x, DynEq eq, ulong[2] dates) {
  TS[] result;
  foreach(var; eq.rhsvars) { 
    result ~= window(lag(x[var.name], var.lag), dates[0], dates[1]); 
  }
  return result;
}

DynFit estimate(MTS x, DynEq eq) {
  TS lhs = x.lhs(eq);
  TS[] rhs = x.rhs(eq);
  auto cd = commonDates(rhs, lhs);
  return estimate(x, eq, cd[0], cd[1]);
}

DynFit[] recursive(MTS x, DynEq eq, ulong t0, ulong t1) {
  TS lhs = x.lhs(eq);
  TS[] rhs = x.rhs(eq);
  auto cd = commonDates(lhs, rhs);
  DynFit[] result;
  foreach(end; t0..t1+1) { 
    result ~= x.estimate(eq, cd[0], end); 
  }
  return result;
}

DynFit[] recursive(MTS x, DynEq eq, ulong[2] t0, ulong[2] t1) {
  return recursive(x, eq, t0[0]*x.frequency + t0[1], t1[0]*x.frequency + t1[1]);
}

DynFit[] rolling(MTS x, DynEq eq, ulong t0, ulong t1, ulong k) {
  DynFit[] result;
  foreach(end; t0..t1+1) { 
    result ~= x.estimate(eq, end-k+1, end); 
  }
  return result;
}

DynFit[] rolling(MTS x, DynEq eq, ulong[2] t0, ulong[2] t1, ulong k) {
  auto s = t0[0]*x.frequency+t0[1];
  auto e = t1[0]*x.frequency+t1[1];
  return rolling(x, eq, s, e, k);
}

DynFit estimate(MTS x, DynEq eq, ulong s, ulong e) {
  TS lhs = x.lhs(eq);
  TS[] rhs = x.rhs(eq);
  DoubleMatrix rhsData = regressors(rhs, [s, e], eq.intercept);
  DoubleVector lhsData = window(lhs, s, e).data;
  OlsFit fit = lm(lhsData, rhsData);
  DynFit result;
  foreach(int ii, var; eq.rhsvars) { 
    result.coef ~= DynCoef(var.name, var.lag, fit.coef[ii,0]); 
  }
  if (eq.intercept) { 
    result.intercept = fit.coef[fit.coef.rows-1,0]; 
  } else {
    result.intercept = 0.0;
  }
  result.estStart = s;
  result.estEnd = e;
  result.frequency = x.frequency;
  return result;
}

DynFit estimate(MTS x, DynEq eq, ulong[2] t0, ulong[2] t1) {
  auto s = t0[0]*x.frequency + t0[1];
  auto e = t1[0]*x.frequency + t1[1];
  return estimate(x, eq, s, e);
}

private double fromBack(TS v, ulong k=0) { 
  return v.data[to!int(v.data.length-k+1)]; 
}

double nextPrediction(MTS x, DynFit fit, ulong horizon=1) {
  double result = fit.intercept;
  foreach(var; fit.coef) { 
    result += fromBack(x[var.name], var.lag-1)*var.b; 
  }
  return result;
}

double predict(MTS x, DynFit fit, ulong horizon=1) {
  double result = fit.intercept;
  ulong fcstDate = fit.estEnd + horizon;
  foreach(var; fit.coef) { 
    auto v = x[var.name];
    result += var.b*v[fcstDate-var.lag];
  }
  return result;
}

TS predict(MTS x, DynFit[] fits, ulong horizon=1) {
  double[] result;
  result.reserve(fits.length);
  foreach(fit; fits) { 
    result ~= x.predict(fit); 
  }
  return TS(DoubleVector(result), fits[0].estEnd+horizon, x.frequency);
}

double predict(MTS x, DynEq eq, ulong fcstDate, ulong s) {
  auto estDate = fcstDate - 1;
  auto fit = x.estimate(eq, s, estDate);
  return x.predict(fit);
}

double predict(MTS x, DynEq eq, ulong[2] fcstDate, ulong[2] s) {
  auto t0 = s[0]*x.frequency + s[1];
  auto t1 = fcstDate[0]*x.frequency + fcstDate[1];
  return predict(x, eq, t0, t1);
}

double predict(MTS x, DynEq eq, ulong fcstDate) {
  TS lhs = x.lhs(eq);
  TS[] rhs = x.rhs(eq);
  auto cd = commonDates(rhs, lhs);
  return predict(x, eq, cd[0], fcstDate-1);
}

double predict(MTS x, DynEq eq, ulong[2] date) {
  auto d = date[0]*x.frequency + date[1];
  return predict(x, eq, d);
}

double mse(TS x, TS fcst) {
  return sum((x - fcst)^^2);
}
