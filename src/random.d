
module rtod2.random;
import rtod2.gretl, rtod2.matrix, rtod2.utils;

void setSeed(uint seed) { gretl_rand_set_seed(seed); }
uint getSeed() { return gretl_rand_get_seed(); }

bool usingBM() { return gretl_rand_get_box_muller() > 0; }
void useBM() { gretl_rand_set_box_muller(1); }
void useZiggurat() { gretl_rand_set_box_muller(0); }

uint genInteger(uint k) { return gretl_rand_int_max(k); }

double[] runif(int n, double min=0.0, double max=1.0) {
  auto result = new double[n];
  gretl_rand_uniform_minmax(result.ptr, 0, n-1, min, max);
  return result;
}

double runif() { return gretl_rand_01(); }

double[] rnorm(int n, double mean=0.0, double sd=1.0) {
  auto result = new double[n];
  gretl_rand_normal_full(result.ptr, 0, n-1, mean, sd);
  return result;
}

double rnorm() { return gretl_one_snormal(); }

double rnorm(double mean, double sd) { return mean + sd*gretl_one_snormal(); }

DoubleMatrix rmvnorm(DoubleMatrix mu, DoubleMatrix V) {
  assertR(mu.cols == 1, "rmvnorm: mu needs to have one column");
  assertR(mu.rows == V.rows, "rmvnorm: mu and v need to have the same number of rows");
  assertR(V.rows == V.cols, "rmvnorm: v needs to be square");
  return mu + V.chol*DoubleMatrix(rnorm(mu.rows));
}

DoubleMatrix rmvnorm(DoubleVector mu, DoubleMatrix V) {
  assertR(mu.length == V.rows, "rmvnorm: mu and v need to have the same number of rows");
  assertR(V.rows == V.cols, "rmvnorm: v needs to be square");
  return DoubleMatrix(mu) + V.chol*DoubleMatrix(rnorm(mu.rows));
}

double[] rchisq(int n, double df) {
  auto result = new double[n];
  gretl_rand_chisq(result.ptr, 0, n-1, df);
  return result;
}

double rchisq(double df) {
  double result;
  gretl_rand_gamma(&result, 0, 0, 0.5*df, 2.0);
  return result;
}

double[] rt(int n, double df) {
  auto result = new double[n];
  gretl_rand_student(result.ptr, 0, n-1, df);
  return result;
}

double rt(double df) {
  double result;
  gretl_rand_student(&result, 0, 0, df);
  return result;
}

double[] rf(int n, int df1, int df2) {
  auto result = new double[n];
  gretl_rand_F(result.ptr, 0, n-1, df1, df2);
  return result;
}

double rf(int df1, int df2) {
  double result;
  gretl_rand_F(&result, 0, 0, df1, df2);
  return result;
}

double[] rbinom(int n, int k, double p) {
  auto result = new double[n];
  gretl_rand_binomial(result.ptr, 0, n-1, k, p);
  return result;
}

double rbinom(int k, double p) {
  double result;
  gretl_rand_binomial(&result, 0, 0, k, p);
  return result;
}

double[] rpois(int n, double m) {
  auto result = new double[n];
  gretl_rand_poisson(result.ptr, 0, n-1, &m, 0);
  return result;
}

double[] rpois(double[] m) {
  int n = m.length;
  auto result = new double[n];
  gretl_rand_poisson(result.ptr, 0, n-1, m.ptr, 1);
  return result;
}

double rpois(double m) {
  double result;
  gretl_rand_poisson(&result, 0, 0, &m, 0);
  return result;
}

double[] rweibull(int n, double shape, double scale) {
  double[] result = new double[n];
  gretl_rand_weibull(result.ptr, 0, n-1, shape, scale);
  return result;
}

double rweibull(double shape, double scale) {
  double result;
  gretl_rand_weibull(&result, 0, 0, shape, scale);
  return result;
}

// double[] rgamma(int n, double shape, double scale) {
//   auto result = new double[n];
//   gretl_rand_gamma(result.ptr, 0, n-1, shape, scale);
//   return result;
// }

// double rgramma(double shape, double scale) {
//   return gretl_rand_gamma_one(shape, scale);
// }

\* GretlMatrix riwish(DoubleMatrix S, int nu) {
  assertR(S.rows == S.cols, "riwish: S must be square");
  assertR(nu >= S.rows, "riwish: nu has to be at least as large as the number of rows of S");
  int err;
  auto result = GretlMatrix(inverse_wishart_matrix(S.ptr, nu, &err));
  assertR(err == 0, "riwish: Something went wrong in the call to inverse_wishart_matrix");
  return result;
}
  
GretlMatrix rwish(DoubleMatrix S, int nu) {
  assertR(S.rows == S.cols, "rwish: S needs to be square");
  assertR(nu >= S.rows, "rwish: nu has to be at least as large as the number of rows of S");
  int err;
  auto result = GretlMatrix(inverse_wishart_matrix(S.inv.ptr, nu, &err));
  assertR(err = 0, "rwish: Something went wrong in the call to inverse_wishart_matrix");
  return result;
} */

double[] rged(int n, double nu) {
  auto result = new double[n];
  gretl_rand_GED(result.ptr, 0, 0, nu);
  return result;
}

/*  double[] rbeta(int n, double s1, double s2) {
    auto result = new double[n];
    gretl_rand_beta(result.ptr, 0, n-1, s1, s2);
    return result;
  }
  
  double rbeta(double s1, double s2) {
    double result;
    gretl_rand_beta(&result, 0, 0, s1, s2);
    return result;
  } */
