
module rtod2.reg;
import rtod2.gretl, rtod2.matrix, rtod2.utils, rtod2.vector;
import std.array, std.conv;

struct OlsFit {
  DoubleMatrix coef;
  DoubleMatrix vcv;
  DoubleMatrix resids;
  double s2;
}

double sse(OlsFit fit) {
  double result = 0.0;
  foreach(e; Col(fit.resids, 0)) { result += e*e; }
  return result;
}

OlsFit lm(DoubleMatrix y, DoubleMatrix x) {
  assertR(y.rows == x.rows, "y and x have different number of rows");
  assertR(y.cols == 1, "y can only have one column");
  auto coef = DoubleMatrix(x.cols, 1);
  auto vcv = DoubleMatrix(x.cols, x.cols);
  auto resids = DoubleMatrix(y.rows, 1);
  double s2;
  gretl_matrix_ols(y.ptr, x.ptr, coef.ptr, vcv.ptr, resids.ptr, &s2);
  return OlsFit(coef, vcv, resids, s2);
}

OlsFit lm(DoubleVector y, DoubleMatrix x) {
  return lm(DoubleMatrix(y), x);
}

OlsFit lmSubsample(DoubleMatrix y, DoubleMatrix x, int r1, int r2) {
  return lm(y.copyRows(r1, r2), x.copyRows(r1, r2));
}

OlsFit lmSubsample(DoubleMatrix y, DoubleMatrix x, bool[] subset) {
  assertR(y.rows == x.rows, "y and x have different number of rows");
  assertR(y.rows == subset.length, "y and subset have different number of rows");
  double[] suby;
  suby.reserve(subset.length);
  Row[] subx;
  subx.reserve(subset.length);
  foreach(obs; 0..to!int(subset.length)) {
    if (subset[obs]) {
      suby ~= y[obs,0]);
      subx ~= Row(x,obs);
    }
  }
  return lm(suby.mat, subx.mat);
}

OlsFit lmSubsample(DoubleMatrix y, DoubleMatrix x, int[] observations) {
  assertR(y.rows == x.rows, "y and x have different number of rows"); 
  assertR(y.rows > observations.length, "Selected more observations than there are rows in y");
  double[] suby;
  Row[] subx;
  foreach(obs; observations) {
    suby ~= y[obs,0];
    subx ~= Row(x,obs); 
  }
  return lm(suby.mat, subx.mat);
}
