
module rtod2.gretl;
import rtod2.matrix, rtod2.utils;

extern (C) {
  int gretl_matrix_multiply(const DoubleMatrix * a, const DoubleMatrix * b, DoubleMatrix * c);
  void gretl_matrix_multiply_by_scalar(DoubleMatrix * m, double x);
  int gretl_matrix_cholesky_decomp(DoubleMatrix * a);
  int gretl_matrix_kronecker_product(const DoubleMatrix * A, const DoubleMatrix * B, DoubleMatrix * K);
  int gretl_LU_solve(DoubleMatrix * a, DoubleMatrix * b);
  int gretl_invert_matrix(DoubleMatrix * a);
  double gretl_matrix_determinant(DoubleMatrix * a, int * err);
  double gretl_matrix_log_determinant(DoubleMatrix * a, int * err);
  double gretl_matrix_trace(const DoubleMatrix * m);
  void gretl_matrix_raise(DoubleMatrix * m, double x);
  DoubleMatrix * gretl_matrix_pow(const DoubleMatrix * A, int s, int * err);
  void gretl_matrix_free(DoubleMatrix * m);
  int gretl_matrix_ols (const DoubleMatrix * y, const DoubleMatrix * X, DoubleMatrix * b, DoubleMatrix * vcv, DoubleMatrix * uhat, double * s2);
  void gretl_matrix_print(const DoubleMatrix * m, const char * msg);
  int gretl_matrix_transpose(DoubleMatrix * targ, const DoubleMatrix * src);
        
  void gretl_rand_init();
  void gretl_rand_free();
  void gretl_rand_set_seed(uint seed);
  void gretl_rand_set_multi_seed(const DoubleMatrix * seed);
  uint gretl_rand_int();
  uint gretl_rand_int_max(uint m);
  double gretl_rand_01();
  double gretl_one_snormal();
  void gretl_rand_uniform(double * a, int t1, int t2);
  int gretl_rand_uniform_minmax(double * a, int t1, int t2, double min, double max);
  void gretl_rand_normal(double * a, int t1, int t2);
  int gretl_rand_normal_full(double * a, int t1, int t2, double mean, double sd);
  int gretl_rand_chisq(double * a, int t1, int t2, double v);
  int gretl_rand_student(double * a, int t1, int t2, double v);
  int gretl_rand_F(double * a, int t1, int t2, int v1, int v2);
  int gretl_rand_binomial(double * a, int t1, int t2, int n, double p);
  int gretl_rand_poisson(double * a, int t1, int t2, const double * m, int vec);
  int gretl_rand_weibull(double * a, int t1, int t2, double shape, double scale);
  int gretl_rand_gamma(double * a, int t1, int t2, double shape, double scale);
  double gretl_rand_gamma_one(double shape, double scale);
  int gretl_rand_GED(double * a, int t1, int t2, double nu);
  int gretl_rand_beta(double * x, int t1, int t2, double s1, double s2);
  int gretl_rand_beta_binomial(double * x, int t1, int t2, int n, double s1, double s2);
  DoubleMatrix * halton_matrix(int m, int r, int offset, int * err);
  DoubleMatrix * inverse_wishart_matrix(const DoubleMatrix * S, int v, int * err);
  DoubleMatrix * inverse_wishart_sequence(const DoubleMatrix * S, int v, int replics, int * err);
  void gretl_rand_set_box_muller(int s);
  int gretl_rand_get_box_muller();
  uint gretl_rand_get_seed();
}
