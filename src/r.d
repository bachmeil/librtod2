
module rtod2.r;
import rtod2.matrix, rtod2.utils, rtod2.vector;

import std.array, std.conv, std.exception, std.math, std.stdio, std.string;

struct RVector {
  DoubleVector vec;
  Robj x;
  immutable bool unprotect; 

  this(int n) { 
    Rf_protect(x = Rf_allocVector(14, n));
    vec.data = REAL(x);
    vec.length = n;
    unprotect = true;
  }
  
  // Necessary to copy into memory managed by R
  this(double[] v) {
    Rf_protect(x = Rf_allocVector(14, to!int(v.length)));
    vec.data = REAL(x);
    vec.length = to!int(v.length);
    unprotect = true;
    foreach(ii; 0..to!int(v.length)) { vec.data[ii] = v[ii]; }
  }

  this(DoubleVector v) {
    Rf_protect(x = Rf_allocVector(14, v.length));
    vec.data = REAL(x);
    vec.length = v.length;
    unprotect = true;
    foreach(ii; 0..v.length) { vec.data[ii] = v[ii]; }
  }
  
  this(Appender!(double[]) v) {
    this(v.data);
  }

  // Copies
  this(Col z) {
    Rf_protect(x = Rf_allocVector(14, z.mat.rows));
    vec.data = REAL(x);
    foreach(row; 0..z.mat.rows) { vec.data[row] = z.mat[row, z.col]; }
    vec.length = z.mat.rows;
    unprotect = true;
  }
        
  // Default is for a vector passed as an argument from R.
  // If you are working with a vector that is already protected,
  // you need u=false.
  this(Robj rz, bool u=false) { 
    x = rz; 
    vec.data = REAL(rz);
    vec.length = Rf_length(rz);
    unprotect = u;
  }

  // Copies
  this(RVectorApp z) {
    auto d = z.x.data;
    Rf_protect(x = Rf_allocVector(14, to!int(d.length)));
    vec.data = REAL(x);
    vec.length = to!int(d.length);
    unprotect = true;
    foreach(row; 0..to!int(d.length)) { vec.data[row] = d[row]; }
  }

  alias vec this;

  ~this() { if (unprotect) { Rf_unprotect_ptr(x); } }
        
  RVector dup() { return RVector(Rf_protect(Rf_duplicate(x)), true); }
        
  Robj robj() { return x; }
        
  void print() {
    foreach(obs; 0..length) { write(this[obs], " "); }
    write("\n");
  }
}

struct RIntVector {
  IntVector vec;
  Robj x;
  immutable bool unprotect; 

  this(int n) { 
    Rf_protect(x = Rf_allocVector(13, n));
    vec.data = INTEGER(x);
    vec.length = n;
    unprotect = true;
  }
  
  // Necessary to copy into memory managed by R
  this(int[] v) {
    Rf_protect(x = Rf_allocVector(13, to!int(v.length)));
    vec.data = INTEGER(x);
    vec.length = to!int(v.length);
    unprotect = true;
    foreach(ii; 0..to!int(v.length)) { vec.data[ii] = v[ii]; }
  }
        
  this(Appender!(int[]) v) {
    this(v.data);
  }

  // Default is for a vector passed as an argument from R.
  // If you are working with a vector that is already protected,
  // you need u=false.
  this(Robj rz, bool u=false) { 
    x = rz; 
    vec.data = INTEGER(rz);
    vec.length = Rf_length(rz);
    unprotect = u;
  }

  alias vec this;

  ~this() { if (unprotect) { Rf_unprotect_ptr(x); } }
        
  RIntVector dup() { return RIntVector(Rf_protect(Rf_duplicate(x)), true); }
        
  Robj robj() { return x; }
}

struct RVectorApp {
Appender!(double[]) x;
  
this(int z=4) {
  x = appender(new double[0]);
}
  
  void put(double z) { x.put(z); }
  
  Robj robj() {
    return RVector(x.data).robj;
  }
}

struct RListRange {
  Robj x;
  int length;
  int obs;

  bool empty() { return length == 0; }
  Robj front() { return VECTOR_ELT(x, obs); }
  void popFront() {
    length -= 1;
    obs += 1;
  }
}

struct RList {
  RListRange range;
  immutable bool unprotect;
  
  this(int n) {
    Rf_protect(range.x = Rf_allocVector(19, n));
    range.length = n;
    range.obs = 0;
    unprotect = true;
  }

  this(Appender!(Robj[]) z) {
    auto d = z.data;
    Rf_protect(range.x = Rf_allocVector(19, to!int(d.length)));
    range.length = to!int(d.length);
    range.obs = 0;
    unprotect = true;
  }

  // Default is to assume that x is already protected
  // As it will be if it is passed from R
  // Otherwise set u=true
  this(Robj x, bool u=false) {
    range.x = x;
    range.length = Rf_length(x);
    range.obs = 0;
    unprotect = u;
  }

  ~this() { if (unprotect) { Rf_unprotect_ptr(range.x); } }

  Robj robj() { return range.x; }

  Robj opIndex(string name) { return getListElement(range.x, name); }

  Robj opIndex(int i) { return VECTOR_ELT(range.x, i); }

  void opIndexAssign(Robj z, string name) { setListElement(range.x, name, z); }

  void opIndexAssign(Robj z, int r) { SET_VECTOR_ELT(range.x, r, z); }
}

struct RListApp {
  Appender!(Robj[]) x;

  void put(Robj z) { x.put(z); }
}

string class(Robj x) {
  return dstring(getAttrib(x, "class"));
}

// Convert Robj holding a single string to a D string
// Could do this by setting a default for the other function
// but this is more efficient
string dstring(Robj cstr) {
  return to!string(R_CHAR(cstr));
}

// Pull the ith element from an R string vector and return it as a D string
string dstring(Robj cstr, int i) {
  return to!string(R_CHAR(STRING_ELT(cstr,i)));
}

private int nameIndex(Robj x, string name) {
  int result = -1;
  Robj names = getAttrib(x, "names");
  for (int ii = 0; ii < Rf_length(x); ii++) {
    if (dstring(names, ii) == name) {
      result = ii;
      break;
    }
  }
  return result;
}

Robj getListElement(Robj rlist, string name) {
  auto index = nameIndex(rlist, name);
  if (index > -1) { return VECTOR_ELT(rlist, to!int(index)); } else { return RNil; }
}

void setListElement(Robj rlist, string name, Robj value) {
  auto index = nameIndex(rlist, name);
  assertR(index > -1, "You attempted to set an element of an R list, but that element doesn't exist.");
  SET_VECTOR_ELT(rlist, index, value);
}

struct RNilVector {
  Robj x;
  
  this(int n) {
    Rf_protect(x = Rf_allocVector(0, n));
  }
  
  ~this() { Rf_unprotect_ptr(x); }
  
  Robj robj() { return x; }
}

Robj RNil() { return RNilVector(1).robj; }

struct RMatrix {
  Robj x;
  DoubleMatrix m;
  immutable bool unprotect;
  
  alias m this;

  this(int r, int c) {
    Rf_protect(x = Rf_allocMatrix(14, r, c));
    m.val = REAL(x);
    m.rows = to!int(r);
    m.cols = to!int(c);
    unprotect = true;
  }

  this(Robj rm, bool u=false) {
    assertR(Rf_isMatrix(rm) == 1, "Constructing RMatrix from something not a matrix"); 
    assertR(Rf_isNumeric(rm) == 1, "Constructing RMatrix from something that is not numeric");
    x = rm;
    m.val = REAL(rm);
    m.rows = Rf_nrows(rm);
    m.cols = Rf_ncols(rm);
    unprotect = u;
  }

  ~this() { if (unprotect) { Rf_unprotect_ptr(x); } }

  RMatrix dup() { return RMatrix(Rf_protect(Rf_duplicate(x))); }

  Robj robj() { return x; }
}    

struct RIntMatrix {
  Robj x;
  IntMatrix m;
  immutable bool unprotect;
  
  alias m this;

  this(int r, int c) {
    Rf_protect(x = Rf_allocMatrix(13, r, c));
    m.val = INTEGER(x);
    m.rows = r;
    m.cols = c;
    unprotect = true;
  }

  this(Robj rm, bool u=false) {
    assertR(Rf_isMatrix(rm) == 1, "Constructing RIntMatrix from something not a matrix"); 
    assertR(Rf_isInteger(rm) == 1, "Constructing RIntMatrix from something not int type");
    x = rm;
    m.val = INTEGER(rm);
    m.rows = Rf_nrows(rm);
    m.cols = Rf_ncols(rm);
    unprotect = u;
  }

  ~this() { if (unprotect) { Rf_unprotect_ptr(x); } }

  RMatrix dup() { return RMatrix(Rf_protect(Rf_duplicate(x))); }

  Robj robj() { return x; }
}

void printR(Robj x) {
  Rf_PrintValue(x);
}

Robj rscalar(double x) {
  auto result = RVector(1);
  result[0] = x;
  return result.robj;
}

void printScalar(double x) { Rf_PrintValue(Rf_ScalarReal(x)); }

void printScalar(int x) { Rf_PrintValue(Rf_ScalarInteger(x)); }

struct RString { 
  Robj robj;

  this(string str) {
    Rf_protect(robj = Rf_allocVector(16, 1));
    SET_STRING_ELT(robj, 0, Rf_mkChar((str ~ "\0").dup.ptr));
  }

  alias strvec this;

  ~this() { Rf_unprotect_ptr(strvec); }
 }

Robj getAttrib(Robj x, string attr) {
  return Rf_getAttrib(x, RString(attr));
}

Robj getAttrib(Robj x, RString attr) {
  return Rf_getAttrib(x, attr);
}

void setAttrib(Robj x, string attr, Robj val) {
  Rf_setAttrib(x, RString(attr), val);
}

void setAttrib(Robj x, RString attr, Robj val) {
  Rf_setAttrib(x, attr, val);
}

ulong[3] tsp(Robj rv) {
  RVector tsprop = getAttrib(rv, "tsp").vec;
  ulong[3] result;
  result[0] = lround(tsprop[0]*tsprop[2])+1;
  result[1] = lround(tsprop[1]*tsprop[2])+1;
  result[2] = lround(tsprop[2]);
  return result;
}

void assertR(bool test, string msg) {
  if (!test) { 
    Rf_error( ("Error in D code: " ~ msg ~ "\0").dup.ptr );
  }
}

extern (C) {
  double * REAL(Robj x);
  int * INTEGER(Robj x);
  const char * R_CHAR(Robj x);
  int * LOGICAL(Robj x);
  Robj STRING_ELT(Robj x, int i);
  Robj VECTOR_ELT(Robj x, int i);
  Robj SET_VECTOR_ELT(Robj x, int i, Robj v);
  void SET_STRING_ELT(Robj x, int i, Robj v);
  int Rf_length(Robj x);
  int Rf_ncols(Robj x);
  int Rf_nrows(Robj x);
  
  void Rf_PrintValue(Robj x);
  int Rf_isArray(Robj x);
  int Rf_isInteger(Robj x);
  int Rf_isList(Robj x);
  int Rf_isLogical(Robj x);
  int Rf_isMatrix(Robj x);
  int Rf_isNull(Robj x);
  int Rf_isNumber(Robj x);
  int Rf_isNumeric(Robj x);
  int Rf_isReal(Robj x);
  int Rf_isVector(Robj x);
  int Rf_isVectorList(Robj x);
  Robj Rf_protect(Robj x);
  Robj Rf_unprotect(int n);
  Robj Rf_unprotect_ptr(Robj x);
  Robj Rf_listAppend(Robj x, Robj y);
  Robj Rf_duplicate(Robj x);
  double Rf_asReal(Robj x);
  int Rf_asInteger(Robj x);
  Robj Rf_ScalarReal(double x);
  Robj Rf_ScalarInteger(int x);
  Robj Rf_getAttrib(Robj x, Robj attr);
  Robj Rf_setAttrib(Robj x, Robj attr, Robj val);
  Robj Rf_mkChar(const char * str);
  void Rf_error(const char * msg);
    
  // type is 0 for NILSXP, 13 for integer, 14 for real, 19 for VECSXP
  Robj Rf_allocVector(uint type, int n);
  Robj Rf_allocMatrix(uint type, int rows, int cols);
        
  // I don't use these, and don't know enough about them to mess with them
  // They are documented in the R extensions manual.
  double gammafn(double);
  double lgammafn(double);
  double lgammafn_sign(double, int *);
  double digamma(double);
  double trigamma(double);
  double tetragamma(double);
  double pentagamma(double);
  double beta(double, double);
  double lbeta(double, double);
  double choose(double, double);
  double lchoose(double, double);
  double bessel_i(double, double, double);
  double bessel_j(double, double);
  double bessel_k(double, double, double);
  double bessel_y(double, double);
  double bessel_i_ex(double, double, double, double *);
  double bessel_j_ex(double, double, double *);
  double bessel_k_ex(double, double, double, double *);
  double bessel_y_ex(double, double, double *);
        
        
  /** Calculate exp(x)-1 for small x */
  double expm1(double);
        
  /** Calculate log(1+x) for small x */
  double log1p(double);
        
  /** Returns 1 for positive, 0 for zero, -1 for negative */
  double sign(double x);
        
  /** |x|*sign(y)
   *  Gives x the same sign as y
   */   
  double fsign(double x, double y);
        
  /** R's signif() function */
  double fprec(double x, double digits);
        
  /** R's round() function */
  double fround(double x, double digits);
        
  /** Truncate towards zero */
  double ftrunc(double x);
        
  /** Same arguments as the R functions */ 
  double dnorm4(double x, double mu, double sigma, int give_log);
  double pnorm(double x, double mu, double sigma, int lower_tail, int log_p);
  double qnorm(double p, double mu, double sigma, int lower_tail, int log_p);
  void pnorm_both(double x, double * cum, double * ccum, int i_tail, int log_p); /* both tails */
  /* i_tail in {0,1,2} means: "lower", "upper", or "both" :
     if(lower) return *cum := P[X <= x]
     if(upper) return *ccum := P[X > x] = 1 - P[X <= x] */

  /** Same arguments as the R functions */ 
  double dunif(double x, double a, double b, int give_log);
  double punif(double x, double a, double b, int lower_tail, int log_p);
  double qunif(double p, double a, double b, int lower_tail, int log_p);

  /** These do not allow for passing argument rate as in R 
      Confirmed that otherwise you call them the same as in R */
  double dgamma(double x, double shape, double scale, int give_log);
  double pgamma(double q, double shape, double scale, int lower_tail, int log_p);
  double qgamma(double p, double shape, double scale, int lower_tail, int log_p);
        
  /** Unless otherwise noted from here down, if the argument
   *  name is the same as it is in R, the argument is the same.
   *  Some R arguments are not available in C */
  double dbeta(double x, double shape1, double shape2, int give_log);
  double pbeta(double q, double shape1, double shape2, int lower_tail, int log_p);
  double qbeta(double p, double shape1, double shape2, int lower_tail, int log_p);

  /** Use these if you want to set ncp as in R */
  double dnbeta(double x, double shape1, double shape2, double ncp, int give_log);
  double pnbeta(double q, double shape1, double shape2, double ncp, int lower_tail, int log_p);
  double qnbeta(double p, double shape1, double shape2, double ncp, int lower_tail, int log_p);

  double dlnorm(double x, double meanlog, double sdlog, int give_log);
  double plnorm(double q, double meanlog, double sdlog, int lower_tail, int log_p);
  double qlnorm(double p, double meanlog, double sdlog, int lower_tail, int log_p);

  double dchisq(double x, double df, int give_log);
  double pchisq(double q, double df, int lower_tail, int log_p);
  double qchisq(double p, double df, int lower_tail, int log_p);

  double dnchisq(double x, double df, double ncp, int give_log);
  double pnchisq(double q, double df, double ncp, int lower_tail, int log_p);
  double qnchisq(double p, double df, double ncp, int lower_tail, int log_p);

  double df(double x, double df1, double df2, int give_log);
  double pf(double q, double df1, double df2, int lower_tail, int log_p);
  double qf(double p, double df1, double df2, int lower_tail, int log_p);

  double dnf(double x, double df1, double df2, double ncp, int give_log);
  double pnf(double q, double df1, double df2, double ncp, int lower_tail, int log_p);
  double qnf(double p, double df1, double df2, double ncp, int lower_tail, int log_p);

  double dt(double x, double df, int give_log);
  double pt(double q, double df, int lower_tail, int log_p);
  double qt(double p, double df, int lower_tail, int log_p);

  double dnt(double x, double df, double ncp, int give_log);
  double pnt(double q, double df, double ncp, int lower_tail, int log_p);
  double qnt(double p, double df, double ncp, int lower_tail, int log_p);

  double dbinom(double x, double size, double prob, int give_log);
  double pbinom(double q, double size, double prob, int lower_tail, int log_p);
  double qbinom(double p, double size, double prob, int lower_tail, int log_p);

  double dcauchy(double x, double location, double scale, int give_log);
  double pcauchy(double q, double location, double scale, int lower_tail, int log_p);
  double qcauchy(double p, double location, double scale, int lower_tail, int log_p);
        
  /** scale = 1/rate */
  double dexp(double x, double scale, int give_log);
  double pexp(double q, double scale, int lower_tail, int log_p);
  double qexp(double p, double scale, int lower_tail, int log_p);

  double dgeom(double x, double prob, int give_log);
  double pgeom(double q, double prob, int lower_tail, int log_p);
  double qgeom(double p, double prob, int lower_tail, int log_p);

  double dhyper(double x, double m, double n, double k, int give_log);
  double phyper(double q, double m, double n, double k, int lower_tail, int log_p);
  double qhyper(double p, double m, double n, double k, int lower_tail, int log_p);

  double dnbinom(double x, double size, double prob, int give_log);
  double pnbinom(double q, double size, double prob, int lower_tail, int log_p);
  double qnbinom(double p, double size, double prob, int lower_tail, int log_p);

  double dnbinom_mu(double x, double size, double mu, int give_log);
  double pnbinom_mu(double q, double size, double mu, int lower_tail, int log_p);

  double dpois(double x, double lambda, int give_log);
  double ppois(double x, double lambda, int lower_tail, int log_p);
  double qpois(double p, double lambda, int lower_tail, int log_p);

  double dweibull(double x, double shape, double scale, int give_log);
  double pweibull(double q, double shape, double scale, int lower_tail, int log_p);
  double qweibull(double p, double shape, double scale, int lower_tail, int log_p);

  double dlogis(double x, double location, double scale, int give_log);
  double plogis(double q, double location, double scale, int lower_tail, int log_p);
  double qlogis(double p, double location, double scale, int lower_tail, int log_p);

  double ptukey(double q, double nranges, double nmeans, double df, int lower_tail, int log_p);
  double qtukey(double p, double nranges, double nmeans, double df, int lower_tail, int log_p);
}
