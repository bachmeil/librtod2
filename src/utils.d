
module rtod2.utils;
import rtod2.gretl, rtod2.matrix, rtod2.r, rtod2.random, rtod2.reg, rtod2.vector;

import std.string, std.array, std.stdio, std.conv, std.range, std.format;

struct sexprec {}
alias Robj = sexprec*;

RVector vec(Robj x) {
    return RVector(x);
  }

  RVector vec(T: double)(Robj x) {
    return RVector(x);
  }

  RIntVector vec(T: int)(Robj x) {
    return RIntVector(x);
  }

// This is not yet ready
//  RStringVector vec(T: string)(Robj x) {
//    return RStringVector(x);
//  }

  RMatrix mat(Robj x) {
    return RMatrix(x);
  }

  RMatrix mat(T: double)(Robj x) {
    return RMatrix(x);
  }

  RIntMatrix mat(T: int)(Robj x) {
    return RIntMatrix(x);
  }

  RList rlist(Robj x) {
    return RList(x);
  }

string genRLib(string fun, string libname) {
        return `
        import core.runtime;
        import rtod2.r, rtod2.vector, rtod2.matrix, rtod2.reg, rtod2.gretl, rtod2.random;
        struct DllInfo;
        
        extern (C) {
                void R_init_lib` ~ libname ~ `(DllInfo * info) {
                        Runtime.initialize();
                        gretl_rand_init();
                }
                
                void R_unload_lib` ~ libname ~ `(DllInfo *info) {
                        gretl_rand_free();
                }
                
` ~ fun ~ `}`;
}

int imin(int x, int y) {
        if (x < y) { return x; } else { return y; }
}

int imax(int x, int y) {
        if (x > y) { return x; } else { return y; }
}

ulong imin(ulong x, ulong y) {
        if (x < y) { return x; } else { return y; }
}

ulong imax(ulong x, ulong y) {
        if (x > y) { return x; } else { return y; }
}

double scalar(Robj x) { return Rf_asReal(x); }

int scalar(T: int)(Robj x) { return Rf_asInteger(x); }

long scalar(T: long)(Robj x) { return to!long(x.scalar!int); }

ulong scalar(T: ulong)(Robj x) { return to!ulong(x.scalar!int); }

string scalar(T: string)(Robj x) { return to!string(R_CHAR(STRING_ELT(x,0))); }

Robj robj(double x) {
              auto result = RVector(1);
              result[0] = x;
              return result.robj;
      }
      
      Robj robj(int x) {
              auto result = RIntVector(1);
              result[0] = x;
              return result.robj;
      }

Robj robj(string x) {
  return RString(x).robj;
}

int[] seq(int t0, int t1, int by=1) { return iota(t0, t1+1, by).array; }

immutable double M_E=2.718281828459045235360287471353;
immutable double M_LOG2E=1.442695040888963407359924681002;
immutable double M_LOG10E=0.434294481903251827651128918917;
immutable double M_LN2=0.693147180559945309417232121458;
immutable double M_LN10=2.302585092994045684017991454684; 
immutable double M_PI=3.141592653589793238462643383280;
immutable double M_2PI=6.283185307179586476925286766559; 
immutable double M_PI_2=1.570796326794896619231321691640;
immutable double M_PI_4=0.785398163397448309615660845820;
immutable double M_1_PI=0.318309886183790671537767526745;
immutable double M_2_PI=0.636619772367581343075535053490;
immutable double M_2_SQRTPI=1.128379167095512573896158903122;
immutable double M_SQRT2=1.414213562373095048801688724210;
immutable double M_SQRT1_2=0.707106781186547524400844362105;
immutable double M_SQRT_3=1.732050807568877293527446341506;
immutable double M_SQRT_32=5.656854249492380195206754896838;
immutable double M_LOG10_2=0.301029995663981195213738894724;
immutable double M_SQRT_PI=1.772453850905516027298167483341;
immutable double M_1_SQRT_2PI=0.398942280401432677939946059934;
immutable double M_SQRT_2dPI=0.797884560802865355879892119869;
immutable double M_LN_SQRT_PI=0.572364942924700087071713675677;
immutable double M_LN_SQRT_2PI=0.918938533204672741780329736406;
immutable double M_LN_SQRT_PId2=0.225791352644727432363097614947;
