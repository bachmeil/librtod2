This is a library of functions that help with creating D functions that can be called from R. It is inspired by Rcpp. Initially I wrote the library just for the heck of it, as a way to kill time while waiting for my son at his various sporting activities. I liked the result and now I use it for my econometric research.

Writing up proper documentation hasn't yet been as important as the other things in my life, like spending time with my wife, so I decided to post the code for anyone interested. I will post examples when I get time. After that I will post documentation. I've used this library only on Linux. It also requires Gretl (the development library, not the application) and the standalone R math library. On Ubuntu, the gretl package is libgretl1-dev.

There is a Makefile to show how I build and install the library on my Linux Mint machine. dub is likely a better approach, but I've never used it, and the Makefile approach requires minimal effort on my part.

License: Everything that I've written is GPL (>= 2).