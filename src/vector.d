
module rtod2.vector;
import rtod2.utils;
import std.array, std.conv, std.exception, std.stdio;

struct DoubleVector {
  double * data;
  int length;

  this(int nr) {
    auto temp = new double[nr];
    data = temp.ptr;
    length = nr;
  }

  this(double[] v) {
    data = v.ptr;
    length = to!int(v.length);
  }

  this(Appender!(double[]) v) {
    data = v.data.ptr;
    length = to!int(v.data.length);
  }
  
  this(double * d, int l) {
    data = d;
    length = l;
  }

  double opIndex(int r) {
    return data[r];
  }

  DoubleVector opSlice(int i, int j) {
    return DoubleVector(&data[i], j-i);
  }

  void opIndexAssign(double v, int r) {
    data[r] = v;
  }

  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[1];
    length -= 1;
  }
}

DoubleVector dup(DoubleVector v) {
  double[] result;
  result.reserve(v.length);
  foreach(val; v) { result ~= val; }
  return result.vec;
}

double sum(DoubleVector v) {
  double result = 0.0;
  foreach(val; v) { result += val; }
  return result;
}

double prod(DoubleVector v) {
  double result = 1.0;
  foreach(val; v) { result *= val; }
  return result;
}

double[] array(DoubleVector v) {
  double[] result;
  result.reserve(v.length);
  foreach(val; v) { result ~= val; }
  return result;
}

void print(DoubleVector v) {
  foreach(val; v) { write(val, " "); }
  write("\n");
}

struct IntVector {
  int * data;
  int length;

  this(int nr) {
    auto temp = new int[nr];
    data = temp.ptr;
    length = nr;
  }

  this(int[] v) {
    data = v.ptr;
    length = to!int(v.length);
  }

  this(Appender!(int[]) v) {
    data = v.data.ptr;
    length = to!int(v.data.length);
  }
  
  this(int * d, int l) {
    data = d;
    length = l;
  }

  int opIndex(int r) {
    return data[r];
  }

  IntVector opSlice(int i, int j) {
    return IntVector(&data[i], j-i);
  }

  void opIndexAssign(int v, int r) {
    data[r] = v;
  }

  bool empty() { return length == 0; }
  int front() { return data[0]; }
  void popFront() {
    data = &data[1];
    length -= 1;
  }
}

IntVector dup(IntVector v) {
  int[] result;
  result.reserve(v.length);
  foreach(val; v) { result ~= val; }
  return IntVector(result);
}

int sum(IntVector v) {
  int result = 0;
  foreach(val; v) { result += val; }
  return result;
}

int prod(IntVector v) {
  int result = 1;
  foreach(val; v) { result *= val; }
  return result;
}

int[] array(IntVector v) {
  int[] result;
  result.reserve(v.length);
  foreach(val; v) { result ~= val; }
  return result;
}

void print(IntVector v) {
  foreach(val; v) { write(val, " "); }
  write("\n");
}
